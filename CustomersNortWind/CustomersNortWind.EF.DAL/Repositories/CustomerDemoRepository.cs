﻿using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.Repositories.Common;
using CustomersNortWind.EF.DAL.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CustomersNortWind.EF.DAL.Repositories
{
    class CustomerDemoRepository : BaseRepository, IRepository<CustomerDemographicDataModel, string>
    {

        private readonly Func<CustomerDemographic, CustomerDemographicDataModel> _EntToDm = c => new CustomerDemographicDataModel()
        {
            CustomerDesc = c.CustomerDesc,
            CustomerTypeID = c.CustomerTypeID,
        };
        private readonly Func<CustomerDemographicDataModel, CustomerDemographic> _DMToEnt = c => new CustomerDemographic()
        {
            CustomerDesc = c.CustomerDesc,
            CustomerTypeID = c.CustomerTypeID,
        };


        #region IRepository implementation

        public CustomerDemographicDataModel GetSingle(string id, bool details = true)
        {
            var rez = GetByQuery(p => p.CustomerTypeID == id);
            return rez.Any() ? rez.First() : null;
        }

        public IList<CustomerDemographicDataModel> GetAll(bool details = true)
        {
            return GetByQuery(p => true);
        }
        public IList<CustomerDemographicDataModel> GetByQuery(Func<CustomerDemographicDataModel, bool> predicate, bool details = false)
        {
            using (var context = GetContext())
            {
                return context.CustomerDemographics.Where(demo => predicate(_EntToDm(demo))).Select(_EntToDm).ToArray();
            }
        }


        public int AddOrUpdate(CustomerDemographicDataModel model)
        {
            using (var context = GetContext())
            {
                // check excistance of entity
                var custDemo = context.CustomerDemographics.FirstOrDefault(p => p.CustomerTypeID == model.CustomerTypeID) ?? new CustomerDemographic();
                if (custDemo == null) //add new key
                {
                    model.CustomerTypeID = CreateNewKey(context);
                }
                custDemo = _DMToEnt(model);
                context.CustomerDemographics.Add(custDemo);

                return context.SaveChanges();
            }
        }

        public int Delete(string id)
        {
            using (var context = GetContext())
            {
                var custDemo = context.CustomerDemographics.FirstOrDefault(p => p.CustomerTypeID == id);
                if (custDemo == null) return -1;
                context.CustomerDemographics.Remove(custDemo);
                return context.SaveChanges();
            }
        }
        #endregion


        private string CreateNewKey(NORTHWNDEntities context)
        {
            // todo create new key 'CystomerTypeId' fo customer demographic
            throw new NotImplementedException();
        }

    }
}
