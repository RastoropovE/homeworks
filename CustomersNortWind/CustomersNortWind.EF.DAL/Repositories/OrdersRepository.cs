﻿using System;
using System.Collections.Generic;
using System.Linq;
using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.Repositories.Common;
using CustomersNortWind.EF.DAL.Repositories.Common;
using System.Linq.Expressions;

namespace CustomersNortWind.EF.DAL.Repositories
{
    public class OrdersRepository : BaseRepository, IContextRepository<OrderDataModel, int,NORTHWNDEntities>
    {
        #region  CONVERTERS

        private readonly Func<Order, OrderDataModel> _ordrEntToDm = c => new OrderDataModel()
        {
            CustomerID = c.CustomerID,
            EmployeeID = c.EmployeeID,
            Freight = c.Freight,
            OrderDate = c.OrderDate,
            OrderID = c.OrderID,
            RequiredDate = c.RequiredDate,
            ShipAddress = c.ShipAddress,
            ShipCity = c.ShipCity,
            ShipCountry = c.ShipCountry,
            ShipName = c.ShipName,
            ShipPostalCode = c.ShipPostalCode,
            ShipRegion = c.ShipRegion,
            ShipVia = c.ShipVia,
            ShippedDate = c.ShippedDate
        };

        private readonly  Expression<Func<Order_Detail, OrderDetailDataModel>> _detailEntToDm = c => new OrderDetailDataModel()
        {
          OrderID = c.OrderID,
          Discount = c.Discount,
          ProductID = c.ProductID,
          Quantity = c.Quantity,
          UnitPrice = c.UnitPrice,
        };


        private static void ModelToEntityOrder(Order ent, OrderDataModel mod)
        {
            ent.CustomerID = mod.CustomerID;
            ent.EmployeeID = mod.EmployeeID;
            ent.Freight = mod.Freight;
            ent.OrderDate = mod.OrderDate;
            ent.OrderID = mod.OrderID;
            ent.RequiredDate = mod.RequiredDate;
            ent.ShipAddress = mod.ShipAddress;
            ent.ShipCity = mod.ShipCity;
            ent.ShipCountry = mod.ShipCountry;
            ent.ShipName = mod.ShipName;
            ent.ShipPostalCode = mod.ShipPostalCode;
            ent.ShipRegion = mod.ShipRegion;
            ent.ShipVia = mod.ShipVia;
            ent.ShippedDate = mod.ShippedDate;
            //ent.Order_Details = mod.OrderDetails.Select(c => new Order_Detail()
            //{
            //    OrderID = c.OrderID,
            //    Discount = c.Discount,
            //    ProductID = c.ProductID,
            //    Quantity = c.Quantity,
            //    UnitPrice = c.UnitPrice,
            //}).ToArray();
        }



        #endregion


        public OrderDataModel GetSingle(int id, bool details = true)
        {
            var ordrs = GetByQuery(order => order.OrderID == id, details);
            return ordrs != null ? ordrs.First() : null;
        }
        public OrderDataModel GetSingle(int id, NORTHWNDEntities context, bool details = true)
        {
            var ordrs = GetByQuery(order => order.OrderID == id,context, details);
            return ordrs != null ? ordrs.First() : null;
        }

        public IList<OrderDataModel> GetAll(bool details = true)
        {
            return GetByQuery(p => true, details);
        }
        public IList<OrderDataModel> GetAll(NORTHWNDEntities context, bool details = true)
        {
            return GetByQuery(p => true,context, details);
        }


        public IList<OrderDataModel> GetByQuery(Func<OrderDataModel, bool> predicate, bool details = true)
        {
            using (var context = GetContext())
            {
                return GetByQuery(predicate, context, details);
            }
        }
        public IList<OrderDataModel> GetByQuery(Func<OrderDataModel, bool> predicate, NORTHWNDEntities context, bool details = true)
        {
            var orders = context.Orders.Select(_ordrEntToDm).Where(predicate).ToList();
            if (!details) return orders;
            foreach (var ord in orders)
            {
                ord.OrderDetails = context.Order_Details.Where(p => p.OrderID == ord.OrderID).Select(_detailEntToDm).ToList();
            }
            return orders;
        }

        public int AddOrUpdate(OrderDataModel model)
        {
            using (var context = GetContext())
            {
                return AddOrUpdate(model, context);
            }
        }

        public int AddOrUpdate(OrderDataModel model, NORTHWNDEntities context)
        {
            // check excistance of entity
            var ordr = context.Orders.FirstOrDefault(p => p.OrderID == model.OrderID);
            if (ordr == null) //add
            {
                ordr = new Order();
                SetOrderNewKey(ordr, context);
                model.OrderID = ordr.OrderID;
                context.Orders.Add(ordr);
            }
            ModelToEntityOrder(ordr, model);

            // detail
            //foreach (var od in ordr.Order_Details)
            //{
            //    var ordDet =
            //        context.Order_Details.FirstOrDefault(p => p.OrderID == od.OrderID && p.ProductID == od.ProductID);
            //    if (ordDet == null)
            //    {
            //        context.Order_Details.Add(od);
            //    }
            //}

            return ordr.OrderID;
        }



        public int Delete(int id)
        {
            using (var context = GetContext())
            {
                return Delete(id, context);
            }
        }

        public int Delete(int id, NORTHWNDEntities context)
        {
            var customer = context.Orders.FirstOrDefault(p => p.OrderID == id);
            if (customer == null) return -1;
            context.Orders.Remove(customer);
            return context.SaveChanges();
        }

        private void SetOrderNewKey(Order ordr, NORTHWNDEntities context)
        {
            throw new NotImplementedException();
        }







    }
}
