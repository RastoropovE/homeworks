﻿using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.DataModels.Keys;
using CustomersNortWind.BL.Repositories.Common;
using CustomersNortWind.EF.DAL.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomersNortWind.EF.DAL.Repositories
{
   public  class OrderDetailsRepository : BaseRepository, IContextRepository<OrderDetailDataModel, OrderDetailKey,NORTHWNDEntities>
    {

         private readonly Func<Order_Detail, OrderDetailDataModel> _EntToDm = c => new OrderDetailDataModel()
         {
             Discount = c.Discount,
             OrderID = c.OrderID,
             ProductID = c.ProductID,
             Quantity = c.Quantity,
             UnitPrice = c.UnitPrice,
         };

       private void _DMToEnt(Order_Detail e, OrderDetailDataModel m)
       {
           e.Discount = m.Discount;
           e.OrderID = m.OrderID;
           e.ProductID = m.ProductID;
           e.Quantity = m.Quantity;
           e.UnitPrice = m.UnitPrice;
       }



        #region interface IRepository implementation

        public OrderDetailDataModel GetSingle(OrderDetailKey id, bool details = true)
        {
            var rez = GetByQuery(p => p.OrderID == id.ProductID && p.ProductID == id.ProductID ,details); 
            return rez.Any() ? rez.First() : null;
        }
        public OrderDetailDataModel GetSingle(OrderDetailKey id, NORTHWNDEntities context, bool details = true)
        {
            var rez = GetByQuery(p => p.OrderID == id.ProductID && p.ProductID == id.ProductID,context, details);
            return rez.Any() ? rez.First() : null;
        }


        public IList<OrderDetailDataModel> GetAll(bool details = true)
        {
            return GetByQuery(p => true,details);
        }
        public IList<OrderDetailDataModel> GetAll(NORTHWNDEntities context, bool details = true)
        {
            return GetByQuery(p => true,context, details);
        }


        public IList<OrderDetailDataModel> GetByQuery(Func<OrderDetailDataModel, bool> predicate, bool details = false)
        {
            using (var context = GetContext())
            {
                return GetByQuery(predicate, context, details);
            }
        }

        public IList<OrderDetailDataModel> GetByQuery(Func<OrderDetailDataModel, bool> predicate, NORTHWNDEntities context, bool details = true)
        {
            return context.Order_Details.Where(od => predicate(_EntToDm(od))).Select(_EntToDm).ToArray();
        }

        public int AddOrUpdate(OrderDetailDataModel model)
        {
            using (var context = GetContext())
            {
               AddOrUpdate(model, context);
               return context.SaveChanges();
            }
        }
        public int AddOrUpdate(OrderDetailDataModel model, NORTHWNDEntities context)
        {
            // check excistance of entity
            var ordDet = context.Order_Details.FirstOrDefault(p => p.OrderID == model.OrderID && p.ProductID == model.ProductID);
            if (ordDet == null)
            {
                ordDet = new Order_Detail();
                context.Order_Details.Add(ordDet);
            }
            _DMToEnt(ordDet,model);
            return 1;
        }

        public int Delete(OrderDetailKey id)
        {
            using (var context = GetContext())
            {
                var ordrDet = context.Order_Details.FirstOrDefault(p => p.OrderID == id.OrderID && p.ProductID == id.ProductID); 
                if (ordrDet == null) return -1;
                context.Order_Details.Remove(ordrDet);
                return context.SaveChanges();
            }
        }
        #endregion


        public int Delete(OrderDetailKey id, NORTHWNDEntities context)
        {
            throw new NotImplementedException();
        }

      
    }
}
