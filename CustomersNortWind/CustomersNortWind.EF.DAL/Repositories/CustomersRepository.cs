﻿using CustomersNortWind.BL.Repositories.Common;
using CustomersNortWind.EF.DAL.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using CustomersNortWind.BL.DataModels;
using System.Linq.Expressions;

namespace CustomersNortWind.EF.DAL.Repositories
{
    public class CustomersRepository : BaseRepository, IRepository<CustomerDataModel, string>
    {

        #region  CONVERTERS

        private readonly Expression<Func<Customer, CustomerDataModel>> _custEntToDm = c => new CustomerDataModel()
        {
            Address = c.Address,
            City = c.City,
            CompanyName = c.CompanyName,
            ContactName = c.CompanyName,
            ContactTitle = c.ContactTitle,
            Country = c.Country,
            CustomerID = c.CustomerID,
            Fax = c.Fax,
            Phone = c.Phone,
            PostalCode = c.PostalCode,
            Region = c.Region,
        };

        private static void ModelToEntity(Customer ent, CustomerDataModel mod)
        {
            ent.Address = mod.Address;
            ent.City = mod.City;
            ent.CompanyName = mod.CompanyName;
            ent.ContactName = mod.CompanyName;
            ent.ContactTitle = mod.ContactTitle;
            ent.Country = mod.Country;
            ent.CustomerID = mod.CustomerID;
            ent.Fax = mod.Fax;
            ent.Phone = mod.Phone;
            ent.PostalCode = mod.PostalCode;
            ent.Region = mod.Region;
            ent.CustomerDemographics = mod.Demographics.Select(p => new CustomerDemographic()
            {
                CustomerDesc = p.CustomerDesc,
                CustomerTypeID = p.CustomerTypeID,
            }).ToArray();
        }
        #endregion


        public CustomerDataModel GetSingle(string id, bool details = true)
        {
            var custs = GetByQuery(customer => customer.CustomerID == id, details);
            return custs!=null ? custs.First() : null;
        }

        public IList<CustomerDataModel> GetAll(bool details = true)
        {
            return GetByQuery(c => true, details);
        }

        public int AddOrUpdate(CustomerDataModel model)
        {
            using (var context = GetContext())
            {
                // check excistance of entity
                var cust = context.Customers.FirstOrDefault(p => p.CustomerID == model.CustomerID);
                if (cust == null) //add
                {
                    cust = new Customer();
                    SetCustomerNewKey(cust, context);
                }
                ModelToEntity(cust, model);
                context.Customers.Add(cust);
                
                // demographic
                foreach (var demographic in cust.CustomerDemographics)
                {
                    var demo = context.CustomerDemographics.FirstOrDefault(p => p.CustomerTypeID==demographic.CustomerTypeID);
                    if (demo == null) //add
                    {
                        demo = new CustomerDemographic();
                        SetDemographicNewKey(demo, context);
                    }
                    ModelToEntity(cust, model);
                    cust.CustomerDemographics.Add(demo);
                }

                return context.SaveChanges();
            }
        }


        public int Delete(string id)
        {
            using (var context = GetContext())
            {
                var cust = context.Customers.FirstOrDefault(p => p.CustomerID == id);
                if (cust == null) return -1;
                context.Customers.Remove(cust);
                return context.SaveChanges();
            }
        }


        public IList<CustomerDataModel> GetByQuery(Func<CustomerDataModel, bool> predicate, bool details = true)
        {
            using (var context = GetContext())
            {
                var customers = context.Customers.Select(_custEntToDm).Where(predicate).ToList();
                if (!details) return customers;
                foreach (var cust in customers)
                {
                    cust.Demographics =
                        context.CustomerDemographics.Where(
                            p => p.Customers.Any(_ => _.CustomerID == cust.CustomerID))
                            .Select(p => new CustomerDemographicDataModel()
                            {
                                CustomerDesc = p.CustomerDesc,
                                CustomerTypeID = p.CustomerTypeID,
                            }).ToList();
                }
                return customers;
            }
        }





        #region New key 

        private void SetCustomerNewKey(Customer customer, NORTHWNDEntities context)
        {
            // todo
            throw new NotImplementedException("");
        }

        private void SetDemographicNewKey(CustomerDemographic demo, NORTHWNDEntities context)
        {
            // todo
            throw new NotImplementedException("");

        }

        #endregion



    }
}
