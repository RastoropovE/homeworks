﻿using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.DataModels.Keys;
using CustomersNortWind.BL.Repositories.Common;
using CustomersNortWind.EF.DAL.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomersNortWind.EF.DAL.Services
{
    public class OrdersService:BaseRepository
    {
        private IContextRepository<OrderDataModel, int, NORTHWNDEntities> OrdersRepository;

        private IContextRepository<OrderDetailDataModel, OrderDetailKey, NORTHWNDEntities> OrderDetailsRepository;

        public OrdersService(IContextRepository<OrderDataModel, int, NORTHWNDEntities> orders,
            IContextRepository<OrderDetailDataModel, OrderDetailKey, NORTHWNDEntities> oDetails)
        {
            OrdersRepository = orders;
            OrderDetailsRepository = oDetails;
        }

        public IList<OrderDataModel> GetAllOrders(bool withDetails = false)
        {
            return OrdersRepository.GetAll(withDetails);
        }
        public OrderDataModel GetSingle(int id, bool withDetails = false)
        {
            return OrdersRepository.GetSingle(id,withDetails);
        }

        public IList<OrderDataModel> GetCustomerOrders(CustomerDataModel customer, bool withDetails = true)
        {
            return OrdersRepository.GetByQuery(o => o.CustomerID == customer.CustomerID, withDetails);
        }

        public IList<OrderDetailDataModel> GetOrderDetails(OrderDataModel order)
        {
            var ordr = OrdersRepository.GetSingle(order.OrderID, true);
            return ordr != null ? ordr.OrderDetails.ToArray() : null;
        }

        public int SaveOrder(OrderDataModel order)
        {
            if (order == null) return -1;
            using (var context = GetContext())
            {
                var ordId = OrdersRepository.AddOrUpdate(order, context);

                foreach (var det in order.OrderDetails)
                {
                    det.OrderID = ordId;
                    SaveOrderDetail(det, context);
                }
                return context.SaveChanges();
            }
        }

        private int SaveOrderDetail(OrderDetailDataModel od, NORTHWNDEntities context)
        {
            if (od == null) return -1;
            return OrderDetailsRepository.AddOrUpdate(od,context);
        }


        public int DeleteOrder(OrderDataModel order)
        {
            if (order == null) return -1;
            foreach (var det in order.OrderDetails)
            {
                OrderDetailsRepository.Delete(new OrderDetailKey(det.OrderID, det.ProductID));
            }
            return OrdersRepository.Delete(order.OrderID);
        }



    }
}
