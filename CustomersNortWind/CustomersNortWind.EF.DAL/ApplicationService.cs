﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.Repositories.Common;
using CustomersNortWind.BL.Services;
using CustomersNortWind.EF.DAL.Repositories;

namespace CustomersNortWind.EF.DAL
{
   public class ApplicationService:IAppService
    {
        private IRepository<CustomerDataModel,  string> Customers { get; set; }
        private IRepository<OrderDataModel,  int> Orders { get; set; }


        public ApplicationService()
        {
            Customers  = new CustomersRepository();
            Orders = new OrdersRepository();
            
        }

        public IList<CustomerDataModel> GetCustomers()
        {
            return Customers.GetAll();
        }

        public IList<OrderDetailDataModel> GetDetailsByOrder(OrderDetailDataModel order)
        {
            throw new NotImplementedException();
        }

        public IList<CustomerDemographicDataModel> GetCustomerGemographics(CustomerDataModel customer)
        {
            throw new NotImplementedException();
        }

        public IList<OrderDataModel> GetCustomerOrders(CustomerDataModel customer)
        {
            return Orders.GetByQuery(order => order.CustomerID == customer.CustomerID, true);
        }




    }
}
