﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomersNortWind.BL.DataModels;
using CustomersNortWind.EF.DAL;
using CustomersNortWind.BL.Services;

namespace CustomersNortWind.WPF.ViewModels
{
    class MainViewModel:BaseViewModel
    {
        
        private CustomerDataModel CustomersService;

        private CustomerDataModel _selectedCustomer;

        public MainViewModel()
        {
            //OrdersService = new ApplicationService();
            //Customers = new ObservableCollection<CustomerDataModel>();
            //foreach (var customerDataModel in OrdersService.GetCustomers())
            //{
            //    Customers.Add(customerDataModel);
            //}
            //Orders= new ObservableCollection<OrderDataModel>();
            //Orders.CollectionChanged += (sender, args) => {args.Action == };
        }


        public ObservableCollection<CustomerDataModel> Customers { get; set; }

        public CustomerDataModel SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                _selectedCustomer = value;
                OnPropertyChanged("SelectedCustomer");
                UpdateOrders();
            }
        }

        private void UpdateOrders()
        {
            //if (SelectedCustomer == null) return;
            //Orders.Clear();
            //foreach (var orderDataModel in OrdersService.GetCustomerOrders(SelectedCustomer))
            //{
            //    Orders.Add(orderDataModel);
            //}
        }

        public ObservableCollection<OrderDataModel> Orders { get; set; }




    }
}
