﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomersNortWind.BL.DataModels.Keys
{
    public class OrderDetailKey
    {
        public int OrderID { get; private set; }
        public int ProductID { get;private set; }

        public OrderDetailKey(int orderid, int prodId)
        {
            OrderID = orderid;
            ProductID = prodId;
        }

    }
}
