﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomersNortWind.BL.DataModels
{
    public partial class CustomerDemographicDataModel
    {
        public string CustomerTypeID { get; set; }
        public string CustomerDesc { get; set; }

        public IList<CustomerDataModel> Customers { get; set; } 


    }
}
