﻿using System;
using System.Collections.Generic;
using CustomersNortWind.BL.DataModels;

namespace CustomersNortWind.BL.Repositories.Common
{
    public interface IContextRepository<TDataModel, in TKey, TContext>:IRepository<TDataModel,TKey>
    {

        TDataModel GetSingle(TKey id,TContext context, bool details = true);
        IList<TDataModel> GetAll(TContext context,bool details=true);
        int AddOrUpdate(TDataModel model, TContext context);
        int Delete(TKey id, TContext context);
        IList<TDataModel> GetByQuery(Func<TDataModel,bool> predicate,TContext context, bool details = true);

    }

}
