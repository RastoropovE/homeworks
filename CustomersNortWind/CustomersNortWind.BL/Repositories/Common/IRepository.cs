﻿using System;
using System.Collections.Generic;
using CustomersNortWind.BL.DataModels;

namespace CustomersNortWind.BL.Repositories.Common
{
   public  interface IRepository<TDataModel, in TKey>
    {

        TDataModel GetSingle(TKey id, bool details = true);
        IList<TDataModel> GetAll(bool details=true);
        int AddOrUpdate(TDataModel model);
        int Delete(TKey id);

        IList<TDataModel> GetByQuery(Func<TDataModel,bool> predicate, bool details = true);

    }

}
