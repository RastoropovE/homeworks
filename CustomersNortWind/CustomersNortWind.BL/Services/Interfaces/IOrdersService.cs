﻿using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomersNortWind.BL.DataModels.Keys;

namespace CustomersNortWind.BL.Services
{
    public interface IOrdersService
    {
        IRepository<OrderDataModel, int> Orders{get;set;}
        IRepository<OrderDetailDataModel, OrderDetailKey> OrderDetails { get; set; }



        IList<OrderDataModel> GetAllOrders( bool withDetails = false);
        IList<OrderDataModel> GetCustomerOrders(CustomerDataModel customer, bool withDetails = true);
        
        IList<OrderDetailDataModel> GetOrderDetails(OrderDataModel order);


        int SaveOrder(OrderDataModel order);
        int DeleteOrder(OrderDataModel order);


    }
}
