﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomersNortWind.BL.DataModels;

namespace CustomersNortWind.BL.Services
{
   public  interface IAppService
    {
        IList<CustomerDataModel> GetCustomers();

        IList<OrderDetailDataModel> GetDetailsByOrder(OrderDetailDataModel order);
        IList<CustomerDemographicDataModel> GetCustomerGemographics(CustomerDataModel customer);
        IList<OrderDataModel> GetCustomerOrders(CustomerDataModel customer);



    }
}
