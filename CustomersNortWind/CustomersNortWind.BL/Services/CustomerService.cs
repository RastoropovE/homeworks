﻿using CustomersNortWind.BL.DataModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomersNortWind.BL.Repositories.Common;

namespace CustomersNortWind.BL.Services
{
    public class CustomerService
    {

        IRepository<CustomerDataModel, string> CustomerRepository;

        public CustomerService(IRepository<CustomerDataModel, string> custRep)
        {
            CustomerRepository = custRep;
        }

        public IList<CustomerDemographicDataModel> GetCustomerDemographic(CustomerDataModel customer)
        {
            if(customer==null)return null;
            var c = CustomerRepository.GetSingle(customer.CustomerID, true);
            return c == null ? null : c.Demographics;

        }

        public IList<CustomerDataModel> GetAllCustomers()
        {
            return CustomerRepository.GetAll();
        }

        public CustomerDataModel GetSingle(string id, bool details)
        {
            return  CustomerRepository.GetSingle(id,details);
        }

        public IList<CustomerDataModel> Get(Func<CustomerDataModel, bool> func, bool details)
        {
            return CustomerRepository.GetByQuery(func, details);
        }


    }
}
