﻿using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.DataModels.Keys;
using CustomersNortWind.BL.Repositories.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomersNortWind.BL.Services
{
    public class OrdersService
    {
        private readonly IRepository<OrderDataModel, int> OrdersRepository;

        private IRepository<OrderDetailDataModel, OrderDetailKey> OrderDetailsRepository;

        public OrdersService(IRepository<DataModels.OrderDataModel, int> orders,
             IRepository<DataModels.OrderDetailDataModel, OrderDetailKey> oDetails)
        {
            OrdersRepository = orders;
            OrderDetailsRepository = oDetails;
        }

        public IList<OrderDataModel> GetAllOrders(bool withDetails = false)
        {
            return OrdersRepository.GetAll(withDetails);
        }

        public IList<OrderDataModel> GetCustomerOrders(CustomerDataModel customer, bool withDetails = true)
        {
            return OrdersRepository.GetByQuery(o => o.CustomerID == customer.CustomerID, withDetails);
        }

        public IList<OrderDetailDataModel> GetOrderDetails(OrderDataModel order)
        {
            var ordr = OrdersRepository.GetSingle(order.OrderID, true);
            return ordr != null ? ordr.OrderDetails.ToArray() : null;
        }

        public int SaveOrder(OrderDataModel order)
        {
            if(order==null) return -1;
            foreach (var det in order.OrderDetails)
            {
                SaveOrderDetail(det);
            }
            return OrdersRepository.AddOrUpdate(order);

        }
        public int SaveOrderDetail(OrderDetailDataModel od)
        {
            if (od == null) return -1;
            return OrderDetailsRepository.AddOrUpdate(od);
        }


        public int DeleteOrder(OrderDataModel order)
        {
            if (order == null) return -1;
            foreach (var det in order.OrderDetails)
            {
                OrderDetailsRepository.Delete(new OrderDetailKey(det.OrderID, det.ProductID));
            }
            return OrdersRepository.Delete(order.OrderID);
        }



    }
}
