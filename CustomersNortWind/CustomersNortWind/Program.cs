﻿using CustomersNortWind.BL.DataModels;
using CustomersNortWind.BL.Services;
using CustomersNortWind.EF.DAL;
using CustomersNortWind.EF.DAL.Repositories;
using CustomersNortWind.EF.DAL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CustomersNortWind
{
    class Program
    {
   
        static void Main(string[] args)
        {
           var  orderService = new CustomersNortWind.EF.DAL.Services.OrdersService(new OrdersRepository(), new OrderDetailsRepository());
             var order = orderService.GetSingle(10248, true);

           order.ShipName = "Vins et alcools Chevalier2";
           var det = new OrderDetailDataModel()
           {
               Discount = 1,
               ProductID = 10,
               Quantity = 14,
               UnitPrice = 12,
           };
           order.OrderDetails.Add(det);

           orderService.SaveOrder(order);

        }
        
    }
}
