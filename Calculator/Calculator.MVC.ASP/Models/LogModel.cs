﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Calculator.CommonBL.DataModels;

namespace Calculator.MVC.ASP.Models
{
    public class LogModel
    {

        public string ResultString { get; set; }

        /// <summary>
        /// if user is null put here 'Anonoim'
        /// </summary>
        public string UserLogin { get; set; }
        /// <summary>
        /// User id or if user is null put here '-1'
        /// </summary>
        public int userId { get; set; }

        public int LogId { get; set; }
        public DateTime CalculateData { get; private set; }
        

    }
}