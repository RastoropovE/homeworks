﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Services;
using Calculator.EF.DAL.Repository;
using Calculator.MVC.ASP.Models;
using Microsoft.AspNet.Identity;
using Calculator.MVC.ASP.Services;

namespace Calculator.MVC.ASP.Controllers
{
    public class UserController : Controller
    {
        UserService _userService= new UserService(new UsersRepository());
        LogService _logsService = new LogService(new LogRepository());

        //
        // GET: /User/
        public ActionResult Index()
        {
            if (!CheckAuthorization()) return RedirectToAction("LogIn");

            var user = UserCookieService.GetUser(Request);
           // ViewBag.LogCount = 

            return View();
        }

        [HttpGet]
        public ActionResult Registration()
        {
            if (CheckAuthorization()) return RedirectToAction("Index");
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(RegisterViewModel model)
        {

            if (!ModelState.IsValid) return View(model);
           

            var loginIsUniq = _userService.ChechLoginUniqness(model.Login);
            if (loginIsUniq)
            {
                var newUser = new UserDataModel()
                {
                    Login = model.Login,
                    Password = model.Password,
                    LastName = model.LastName,
                    FirstName = model.FirstName
                };
                if (_userService.RegisterNewUser(newUser))
                {
                    ViewBag.Message = string.Format(" {0}, now you can log in",model.Login);
                    return View(new RegisterViewModel());
                }
            }
            
            ModelState.AddModelError("Login","Choose another login please");

            return View(model);
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            if (CheckAuthorization()) return RedirectToAction("Index");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogIn(string login, string password, bool rememberMe)
        {
            if (CheckAuthorization()) return RedirectToAction("Index");

            if(string.IsNullOrEmpty(login))
                ModelState.AddModelError(String.Empty, "Login is required");
            if (string.IsNullOrEmpty(password))
                ModelState.AddModelError(String.Empty, "Password is required");


            var user = _userService.GetSingle(login);
            if (user != null)
            {
                UserCookieService.SetCookies(Response, user, rememberMe);
                return RedirectToAction("Index");
            }
            ModelState.AddModelError(String.Empty,"Usrename or/and password is not correct");

            return View();
        }

        public ActionResult LogOff()
        {
            UserCookieService.Reset(Response, Request);
            return RedirectToAction("Index");
        }


        public bool CheckAuthorization()
        {
            var userfrCookies = UserCookieService.GetUser(Request);
            if(userfrCookies!=null)
            {
                var user =  _userService.GetSingle(userfrCookies.Login);
                if (user != null && user.Password == userfrCookies.Password)
                    return true;
                UserCookieService.Reset(Response, Request);
            }
            return false;
        }


	}
}