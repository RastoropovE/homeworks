﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Services;
using Calculator.CommonBL.Services.Calculator;
using Calculator.EF.DAL.Repository;
using Calculator.CommonBL.Core;

namespace Calculator.MVC.ASP.Controllers
{
    public class HomeController : Controller
    {
        private BaseCalc _calk;
        private LogService _logService;


        public HomeController()
        {
            _logService = new LogService(new LogRepository());
            _calk = new BaseCalc(new StandartElementsFactory());
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Result(string expr,bool saveLog)
        {

            var rez = _calk.GetResult(expr);

            if (saveLog)
                _logService.AddLog(new LogDataModel(rez));

            return PartialView(rez);

        }
 


    }
}