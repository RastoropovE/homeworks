﻿using System.Linq;
using Calculator.CommonBL.Services;
using Calculator.EF.DAL.Repository;
using System.Web.Mvc;

namespace Calculator.MVC.ASP.Controllers
{
    public class LogsController : Controller
    {

        LogService _logsSevice;
        public LogsController()
        {
            _logsSevice = new LogService(new LogRepository());
        }
 

        public ActionResult Index()
        {
            return View(_logsSevice.GetAll().ToArray());
        }

        [HttpPost]
        public ActionResult Index(int userId)
        {
            return View(_logsSevice.GetUserLogs(userId!=-1?(int?)userId:null).ToArray());
        }

    }
}