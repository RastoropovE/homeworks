﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Calculator.CommonBL.DataModels;

namespace Calculator.MVC.ASP.Services
{
    public class UserCookieService
    {
        private static string _cookieName = "xxxxxx";

        public static void SetCookies(HttpResponseBase response, UserDataModel user, bool remember)
        {
            var cookie = new HttpCookie(_cookieName)
            {
                Expires = DateTime.Now.Add(new TimeSpan(remember?7:0,0,30,0))
            };
            cookie.Values.Add("Login",user.Login);
            cookie.Values.Add("Id", user.UserId.ToString());
            cookie.Values.Add("Hash", user.Password);

            response.SetCookie(cookie);

        }

        public static bool GetUser(HttpRequestBase request, ref UserDataModel user)
        {
            user = GetUser(request);
            return user != null;
        }
        public static UserDataModel GetUser(HttpRequestBase request)
        {
            var cookie = request.Cookies[_cookieName];
            if(cookie==null) return null;

            var idstr = cookie.Values["Id"];
            int id;
            if (int.TryParse(idstr, out id))
            {
                return new UserDataModel()
                {
                    UserId = id,
                    Login =  cookie.Values["Login"],
                    Password = cookie.Values["Password"],
                };
            }
            return null;
        }



        public static bool Reset(HttpResponseBase response, HttpRequestBase request)
        {
            if (request.Cookies[_cookieName] == null) return false;
            var c = new HttpCookie(_cookieName) {Expires = DateTime.Now.AddDays(-1)};
            response.Cookies.Add(c);
            return true;
        }


    }
}