﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Calculator.MVC.ASP.Startup))]
namespace Calculator.MVC.ASP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
