﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Repository;

namespace Calculator.CommonBL.Services
{
    public class LogService
    {
        readonly IRepository<LogDataModel> _logRepository;

        public LogService(IRepository<LogDataModel> userRepository)
        {
            _logRepository = userRepository;
        }

        public IList<LogDataModel> GetUserLogs(UserDataModel user)
        {
            return _logRepository.GetByQuery(p =>p.User!=null&& p.User.UserId == user.UserId);

        }

        public IList<LogDataModel> GetUserLogs(int?  userId)
        {
            return _logRepository.GetByQuery(p => p.UserId == userId);

        }

        public IList<LogDataModel> GetAll()
        {
            return _logRepository.GetAll();
        } 


        public bool AddLog(LogDataModel log)
        {
            var i = _logRepository.AddOrUpdate(log);
            return i > 0;
        }

        public bool DeleteLog(int id)
        {       
            return _logRepository.Delete(id);
        }


    }
}
