﻿using System.CodeDom;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.CommonBL.Services
{
    public class UserService
    {

        public delegate void UserChangedHandler();

        public event UserChangedHandler UserChanged;

        public UserDataModel CurrentUser
        {
            get { return _currentUser; }
            private set
            {
                _currentUser = value;
                var handler = UserChanged;
                if(handler!=null)
                    handler.Invoke();
            }
        }

        IRepository<UserDataModel> _userRepository;
        private UserDataModel _currentUser;

        public UserDataModel GetSingle(string login)
        {
            var user = _userRepository.GetByQuery(p => p.Login == login);
            return user != null ? user.FirstOrDefault() : null;
        }
        public UserDataModel GetSingle(int id)
        {
            return _userRepository.GetSingle(id);
        }

        public UserService(IRepository<UserDataModel> userRepository)
        {
            _userRepository = userRepository;
        }

        public bool RegisterNewUser(UserDataModel user)
        {
            try
            {
                _userRepository.AddOrUpdate(user);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }



        public bool SetCurrentUser(int userId)
        {
            var user = _userRepository.GetSingle(userId);
            if (user == null) return false;
            CurrentUser = user;
            return true;
        }

        public bool LogOut(int userId)
        {
            CurrentUser = null;
            return true;

        }




        /// <summary>
        /// Returns true if login is unique
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public bool ChechLoginUniqness(string login)
        {

            var user = _userRepository.GetByQuery(p => p.Login == login).FirstOrDefault();
            return user == null;

        }
    }


}
