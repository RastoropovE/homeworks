﻿using System;
using System.Linq;
using Calculator.CommonBL.Core;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Exceptions;
using System.Collections.Generic;

namespace Calculator.CommonBL.Services.Calculator
{

    public  class BaseCalc
    {

        private PostfixNotationConverter _calk;

        public BaseCalc(IPostfixElementsFactory set)
        {
            _calk = new PostfixNotationConverter(set);
        }

        public IEnumerable<ValidateElementResult> GetExpressionElements(String expression)
        {
            var rez = _calk.GetElements(expression);
            return rez.Select(p => new ValidateElementResult(p, _calk.CheckElement(p))
              {

              });
        }
        
        public virtual ResultDataModel GetResult(string expression)
        {
            var rez = new ResultDataModel();
            try
            {
                rez.Message = expression;
                rez.Rezult = _calk.GetResult(expression);
            }
            catch(InvalidOperatorException ex )
            {
                rez.IsError = true;
                rez.ErrorDetails = ex.ToString();
            }
            catch(Exception e)
            {
                rez.IsError = true;
                rez.ErrorDetails = e.Message;
            }
            return rez;
        }


        //public abstract IList<string> ParseExpression(string expression);
        //public IEnumerable<string> ConvertToPostfixNotation(string expression)
        //{
        //  return _calk.ConvertToPostfixNotation(expression);
        //}


    }
}
