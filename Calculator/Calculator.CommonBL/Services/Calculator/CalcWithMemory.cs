﻿using Calculator.CommonBL.Core;
namespace Calculator.CommonBL.Services.Calculator
{
   public  class CalcWithMemory:BaseCalc
    {

        public double MemoryValue { get; private set; }

        public void MempryPlus(double value)
        {
            MemoryValue += value;
        }

        public void MemoryMinus(double value)
        {
            MemoryValue -= value;
        }

        public void MemoryReset()
        {
            MemoryValue = 0;
        }

        public CalcWithMemory(IPostfixElementsFactory set):base(set)
        {        }

    }

}
