﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Core;

namespace Calculator.CommonBL.Services.Calculator
{

    public  class MultiUserCalc : CalcWithMemAndLogs
    {

        public  UserService UserService { get; private set; }

        public MultiUserCalc(LogService logService, UserService userService , IPostfixElementsFactory set)
            : base(logService ,set)
        {
            UserService = userService;
            
        }


        protected sealed override bool AddLog(LogDataModel log)
        {
            log.User = UserService.CurrentUser;
            return base.AddLog(log);
        }
        protected  bool AddLog(LogDataModel log, UserDataModel model)
        {
            log.User = UserService.CurrentUser;
            return base.AddLog(log);
        }

    }
}
