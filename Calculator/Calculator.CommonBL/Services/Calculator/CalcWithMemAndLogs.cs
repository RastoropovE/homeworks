﻿using System.Collections.Generic;
using Calculator.CommonBL.DataModels;

namespace Calculator.CommonBL.Services.Calculator
{
    public class CalcWithMemAndLogs:CalcWithMemory
    {
        public bool EnableLogs { get; set; }

        private LogService _logService;
        private LogService logService;


        public CalcWithMemAndLogs(LogService logService, Core.IPostfixElementsFactory set):base(set)
        {
            // TODO: Complete member initialization
            this.logService = logService;

        }


        public new ResultDataModel GetResult(string expression)
        {
            var rez =  base.GetResult(expression);
            AddLog(new LogDataModel(rez));
            return rez;
        }


        public virtual IList<LogDataModel> GetLogs()
        {
           var logs =  _logService.GetAll();
           //todo log filter
            return logs;
        }

        public bool RemoveLog(LogDataModel log)
        {
            return _logService.DeleteLog(log.LogId);
        }

        protected virtual bool  AddLog(LogDataModel log)
        {
            if (EnableLogs && log != null)
                return _logService.AddLog(log);
            return false;
        }


    }

}
