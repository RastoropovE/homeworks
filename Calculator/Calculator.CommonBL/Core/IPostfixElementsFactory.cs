﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.CommonBL.Core
{
    public interface IPostfixElementsFactory
    {
        /// <summary>
        /// Return postfix element for string value
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        PostfixElement GetElement(string p);

        /// <summary>
        /// Check is the input string a valid element
        /// </summary>
        /// <param name="el"></param>
        /// <returns></returns>
        bool IsValidElement(string el);

        string[] GetAllovedElements();
    }
}
