﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator.CommonBL.Core
{
    public class StandartElementsFactory:IPostfixElementsFactory
    {
       //          priority                 left - min                             right - max
       //private readonly String[] _operators = { "(", ")", "+", "-", "*", "/", "^", "√" };
       // public string[] Operators{  get; set; }

        
        Dictionary<string,PostfixElement>  elements = new Dictionary<string, PostfixElement>()
        {
             {"(", new PostfixElement(true,0) },
             {")", new PostfixElement(false,0) },

             {"+", new PostfixElement(2, 1){ Operation = d => d[0] + d[1]  } },
             {"-", new PostfixElement(2, 1){ Operation = d => d[1] - d[0]  } },
             {"/", new PostfixElement(2, 2){ Operation = d => d[1] / d[0]  } },
             {"*", new PostfixElement(2, 2){ Operation = d => d[0] * d[1]  } },

             {"^", new PostfixElement(2, 3){Operation = d => Math.Pow(d[1], d[0])} },

             // unary
             {"√", new PostfixElement(1, 4){Operation = d => Math.Sqrt(d[0])} }, 
             {"cos", new PostfixElement( 1, 4){Operation = d => Math.Cos(d[0])} }, 
             {"sin", new PostfixElement( 1, 4){Operation = d => Math.Cos(d[0])} }, 


        };


        public  PostfixElement GetElement(string p)
        {
            double num;
            if (double.TryParse(p,out num)) //operator
            {
                var val = double.Parse(p);
                return new PostfixElement(val);
            }
            var postfixElement = GetOperator(p);
            return postfixElement;
        }

        public bool IsValidElement(string el)
        {
            double num;
            if (double.TryParse(el, out num)) //operator
                return true;
            else
                return elements.Keys.Contains(el);
        }

        private PostfixElement GetOperator(string p)
        {
            var rex = elements.Where(pair => pair.Key == p).Select(pair => pair.Value);
            return rex.SingleOrDefault();
        }


        public string[] GetAllovedElements()
        {
            return elements.Keys.ToArray();
        }
    }
}
