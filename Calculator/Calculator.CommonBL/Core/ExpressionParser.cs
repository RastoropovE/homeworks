﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator.CommonBL.Core
{
    public class ExpressionParser
    {
        private char[] unitOperation = new[] {'/', '-', '*', '+', '(', ')','√'};
        private string[] elements;

        public ExpressionParser(IPostfixElementsFactory factory)
        {
            elements = factory.GetAllovedElements();
        }

        public IEnumerable<string> ParseExpression(string input)
        {
            var parts = new List<string>();

            ParseMethod1(input, parts);

            return parts;
        }

        private void ParseMethod2(string input, List<string> parts)
        {
            var part = string.Empty + input[0];
            var splitted = input.Trim().Split(elements, StringSplitOptions.None).ToList();
            parts = splitted;
        }

        private void ParseMethod1(string input, List<string> parts)
        {
            var part = string.Empty + input[0];

            for (var i = 1; i < input.Length; i++)
            {
                if (unitOperation.Contains(input[i]) || (i != 0) && unitOperation.Contains(input[i - 1]))
                {
                    parts.Add(part);
                    part = string.Empty;
                }
                part += input[i];
            }
            parts.Add(part); // last part

            SplitDoublePlusMinus(parts);
        }

        private void SplitDoublePlusMinus(List<string> parts)
        {
            //todo
        }



        private bool IsDigit(char c)
        {
            return char.IsDigit(c) || c == ','; //todo ',' or '.'
        }

    }
}
