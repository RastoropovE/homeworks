﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.CommonBL.Core
{
    public class ValidateElementResult
    {

        public bool IsValidElement { get;  set; }
        public string Element { get;  set; }
        public ValidateElementResult(string element,bool result)
        {
            IsValidElement = result;
            Element = element;
        }

    }
}
