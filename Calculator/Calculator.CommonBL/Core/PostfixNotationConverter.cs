﻿using System;
using System.Collections.Generic;
using System.Linq;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Exceptions;

namespace Calculator.CommonBL.Core
{
   public  class PostfixNotationConverter
   {
       private IPostfixElementsFactory _pr;

       private ExpressionParser _parser;


       public IEnumerable<string> GetElements(string expression)
       {
           return _parser.ParseExpression(expression);
       }

       public PostfixNotationConverter(IPostfixElementsFactory set)
       {
           _pr = set;
           _parser = new ExpressionParser(set);
       }


       public bool CheckElement(string el)
       {
           return _pr.IsValidElement(el);
       }


       public IEnumerable<PostfixElement> ConvertToPostfixNotation(string input)
       {
           var expressionElements = _parser.ParseExpression(input).ToArray();
          return ConvertToPostfixNotation(expressionElements);
       }

       private IEnumerable<PostfixElement> ConvertToPostfixNotation(string[] input)
       {
           var outputSeparated = new List<PostfixElement>();
           var stack = new Stack<PostfixElement>();

           for (int i = 0; i < input.Count(); i++)
           {
               var c = _pr.GetElement(input[i]);
               if (c == null) throw new InvalidOperatorException(i+1,input[i]);
               if (c.ElementType == ElType.Operator)
               {
                   if (stack.Count > 0 && c.ElementType != ElType.LeftB)
                   {
                       if (c.ElementType == ElType.RightB)
                       {
                           var s = stack.Pop();
                           while (s.ElementType != ElType.LeftB)
                           {
                               outputSeparated.Add(s);
                               s = stack.Pop();
                           }
                       }
                       else if (c.Priority > stack.Peek().Priority)
                           stack.Push(c);
                       else
                       {
                           while (stack.Count > 0 && c.Priority <= stack.Peek().Priority)
                               outputSeparated.Add(stack.Pop());
                           stack.Push(c);
                       }
                   }
                   else
                       stack.Push(c);
               }
               else
                   outputSeparated.Add(c);
           }

           if (stack.Count > 0)
               outputSeparated.AddRange(stack);

           return
               outputSeparated.Where(p => p.ElementType != ElType.LeftB)
                   .Where(p => p.ElementType != ElType.RightB)
                   .Select(p => p)
                   .ToArray();
       }



       public double GetResult(string input)
        {
            var stack = new Stack<PostfixElement>();
            var queue = new Queue<PostfixElement>(ConvertToPostfixNotation(input));
            var str = queue.Dequeue();
           if (queue.Count == 0) return str.Value;
            while (queue.Count >= 0)
            {
                if (str.ElementType!=ElType.Operator)
                {
                    stack.Push(str);
                    str = queue.Dequeue();
                }
                else
                {
                    double summ = 0;
                    try
                    {
                        switch (str.Operands)
                        {
                            case 1:
                                {
                                    var a =stack.Pop().Value;
                                    summ = str.Operation(new []{a});
                                    break;
                                }
                            case 2:
                                {
                                    var a = stack.Pop().Value;
                                    var b = stack.Count==0?0:stack.Pop().Value;
                                   // var b = stack.Pop().Value;
                                    summ = str.Operation(new [] { a,b });
                                    break;
                                }    
                        }
                    }
                    catch (Exception ex)
                    {
                        
                    }
                    stack.Push(new PostfixElement(summ));
                    if (queue.Count > 0)
                        str = queue.Dequeue();
                    else
                        break;
                }
            }
            return stack.Pop().Value;
        }



    }
}
