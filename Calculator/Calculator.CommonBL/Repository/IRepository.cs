﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.CommonBL.Repository
{
    public interface IRepository<TDataModel>
    {
        TDataModel GetSingle(int id);
        IList<TDataModel> GetAll();

        int AddOrUpdate(TDataModel model);
        bool Delete(int id);
        IList<TDataModel> GetByQuery(Func<TDataModel, bool> predicate, bool details = true);

    }
}
