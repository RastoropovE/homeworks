﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Calculator.CommonBL.DataModels
{
    [Serializable]
    public class LogDataModel:IEquatable<LogDataModel>
    {
        [Key]
        public int LogId { get; set; }
        public int? UserId { get; set; }

        public UserDataModel User { get; set; }

        public bool IsError { get; set; }
        public string ErrorDetails { get; set; }

        public string Message { get; set; }

        public double Rezult { get; set; }
        public DateTime DateTime { get; private set; }
        public LogDataModel(ResultDataModel result)
        {
            if(result==null) throw  new ArgumentNullException("result");
            IsError = result.IsError;
            Message = result.Message;
            Rezult = result.Rezult;
            ErrorDetails = result.ErrorDetails;

            DateTime = DateTime.Now;
        }

        public LogDataModel()
        {
            LogId = -1;
        }
        public override string ToString()
        {
            return string.Format("{0}({1}) {2} = {3}", LogId, this.DateTime, Message,IsError?"Error": Rezult.ToString());
        }

        /// <summary>
        /// compare datamodels by id
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var l = obj as LogDataModel;
            if (l != null)
                return this.LogId == l.LogId;
            return false;
        }

        public bool Equals(LogDataModel other)
        {
            return other != null && LogId == other.LogId && DateTime.Equals(other.DateTime);
        }
    }

}
