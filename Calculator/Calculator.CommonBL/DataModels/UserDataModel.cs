﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.CommonBL.DataModels
{
    public class UserDataModel
    {
        

        [Key]
        public int UserId { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string Password { get; set; }

        public IList<LogDataModel> Logs { get; set; }

        public UserDataModel()
        {
            UserId = -1;
        }

    }
}
