﻿using System;

namespace Calculator.CommonBL.DataModels
{
    [Serializable]
    public class ResultDataModel
    {
        public bool IsError { get; set; }
        public string ErrorDetails { get; set; }
        public string Message { get; set; }

        public double Rezult { get; set; }

        public override string ToString()
        {
                return string.Format("{0} = {1}", Message, IsError ? ErrorDetails : Rezult.ToString());

        }
    }
}
