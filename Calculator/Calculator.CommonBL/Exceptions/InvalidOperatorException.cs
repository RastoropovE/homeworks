﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.CommonBL.Exceptions
{
    public class InvalidOperatorException:Exception
    {
        public int Position { get; set; }
        public string Operator { get; set; }

        public InvalidOperatorException(int position, string op)
        {
            Position = position;
            Operator = op;            
        }


        public override string ToString()
        {
            return String.Format("Invalid symbol '{0}'. Element  {1} ",Operator,Position);
        }
    }
}
