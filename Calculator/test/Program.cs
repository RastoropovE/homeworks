﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calculator.CommonBL.Core;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Services;
using Calculator.CommonBL.Services.Calculator;
using Calculator.EF.DAL.Repository;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {

            var calc = new CalcWithMemory(new StandartElementsFactory());
            foreach( var el in calc.GetExpressionElements("2+3*s+1*sci(2)")){
                Console.WriteLine(el.Element + " - " + el.IsValidElement);
            }

            while (true)
            {
                var c = calc.GetResult(Console.ReadLine());
                Console.WriteLine(c);
            }

        }



    }
}
