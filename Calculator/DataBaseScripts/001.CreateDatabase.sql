USE master;
GO
IF EXISTS (SELECT * FROM sys.databases WHERE name = 'MultiuserCalc')
BEGIN 
	DROP DATABASE MultiuserCalc;
END
GO

CREATE DATABASE MultiuserCalc;
GO
USE MultiuserCalc;
CREATE TABLE Users(
	[UserId] INT PRIMARY KEY,
	[Login] NVARCHAR(15) NOT NULL,
    [FirstName] NVARCHAR(15) NOT NULL,
    [LastName] NVARCHAR(15) NOT NULL,
    [Password] NVARCHAR(15) NOT NULL,
)
go
CREATE TABLE Logs(
[LogId] INT PRIMARY KEY,
[UserID] INT FOREIGN KEY REFERENCES Users(UserId) NULL,
[IsError] BIT NULL,
[ErrorDetails] NVARCHAR(150) NOT NULL,
[Message] NVARCHAR(100) NOT NULL,
[Result] float NULL,
[DateTime] datetime NOT NULL,
)

