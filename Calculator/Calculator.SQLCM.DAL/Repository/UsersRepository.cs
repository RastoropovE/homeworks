﻿using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.SQLCM.DAL.Repository
{
    public class UsersRepository:IRepository<UserDataModel>
    {

        public UserDataModel GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public IList<UserDataModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public int AddOrUpdate(UserDataModel model)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IList<UserDataModel> GetByQuery(Func<UserDataModel, bool> predicate, bool details = true)
        {
            throw new NotImplementedException();
        }
    }
}
