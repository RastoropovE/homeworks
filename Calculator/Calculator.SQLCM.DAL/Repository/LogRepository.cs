﻿using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.SQLCM.DAL.Repository
{
   public  class LogRepository:IRepository<LogDataModel>
    {
        public LogDataModel GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public IList<LogDataModel> GetAll()
        {
            throw new NotImplementedException();
        }

        public int AddOrUpdate(LogDataModel model)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IList<LogDataModel> GetByQuery(Func<LogDataModel, bool> predicate, bool details = true)
        {
            throw new NotImplementedException();
        }
    }
}
