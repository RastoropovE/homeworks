﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Calc1.BL.Operations;
using ScrumTaskBoard.Classes.Commands;

namespace Calculator.ViewModels
{
    class SimpleCommandViewModel
    {

         public delegate void OnKeyPressedEvent(SimpleOperation  operation);
        // public event keyPressed;
        public event OnKeyPressedEvent KeyPressed ;

        public SimpleCommandViewModel()
        {
            OnKeyPressCommand = new RelayCommand(OnKeyPressedAction);
            ((RelayCommand)OnKeyPressCommand).IsEnabled = true;
        }

        private void OnKeyPressedAction(object o)
        {
            if (!(o is SimpleOperation)) return;
            var i = (SimpleOperation) o;

            if (KeyPressed != null)
                KeyPressed.Invoke(i);

        }

        public ICommand OnKeyPressCommand { get; set; }
    }
}
