﻿using System;
using Calc1.BL.Operations;
using ScrumTaskBoard.ViewModels;

namespace Calculator.ViewModels
{
    class CalculatorViewModel:BaseViewModel
    {
        #region nested views contexts
        private SimpleNumpadViewModel _numpadContext;
        public SimpleNumpadViewModel NumpadContext
        {
            get { return _numpadContext; }
            set
            {
                _numpadContext = value;
                OnPropertyChanged("NumpadContext");
            }
        }
        private SimpleCommandViewModel _commandNumpadViewModel;
        public SimpleCommandViewModel CommandNumpadViewModel
        {
            get { return _commandNumpadViewModel; }
            set
            {
                _commandNumpadViewModel = value;
                OnPropertyChanged("CommandNumpadViewModel");
            }
        }
        #endregion

        Calc1.BL.Calculator _calculator;
        private string _currentDisplay;
        private string _previousDisplay;

        public String CurrentDisplay
        {
            get { return _currentDisplay; }
            set
            {
                _currentDisplay = value;
                OnPropertyChanged("CurrentDisplay");
            }
        }

        public string PreviousDisplay
        {
            get { return _previousDisplay; }
            set
            {
                _previousDisplay = value;
                OnPropertyChanged("PreviousDisplay");
            }
        }

        public CalculatorViewModel()
        {
            _calculator = new Calc1.BL.Calculator();
            NumpadContext = new SimpleNumpadViewModel();
            CommandNumpadViewModel = new SimpleCommandViewModel();

            NumpadContext.KeyPressed+=NumpadContextOnKeyPressed;
            CommandNumpadViewModel.KeyPressed+=CommandNumpadViewModelOnKeyPressed;

        }


        private void CommandNumpadViewModelOnKeyPressed(SimpleOperation operation)
        {
            _calculator.SetAction(p=>p.GetSimple(operation));
            Solve();
                PreviousDisplay = CurrentDisplay;
                CurrentDisplay = "0";
            

        }

        private void NumpadContextOnKeyPressed(char key)
        {
            if (key == '=')
            {
                Solve();
            }
            else
            {
                if (CurrentDisplay == "0")
                    CurrentDisplay = string.Empty;
                CurrentDisplay += key;
            }
        }

        private void Solve()
        {
            if (_calculator.CurrentOperation.IsBinary)
            {
                PreviousDisplay = _calculator.GetResult(GetDouble(PreviousDisplay), GetDouble(CurrentDisplay)).ToString();
                CurrentDisplay = string.Empty;
            }
            else
            {
                PreviousDisplay = _calculator.GetResult(GetDouble(CurrentDisplay)).ToString();
            }

        }


        private double GetDouble( string str)
        {
            double numb;
            double.TryParse(str, out numb);
            return numb;
        }

    }
}
