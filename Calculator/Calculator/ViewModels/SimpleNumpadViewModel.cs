﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ScrumTaskBoard.Classes.Commands;

namespace Calculator.ViewModels
{
    internal class SimpleNumpadViewModel
    {
        public delegate void OnKeyPressedEvent(char  key);
        // public event keyPressed;
        public event OnKeyPressedEvent KeyPressed ;

        public SimpleNumpadViewModel()
        {
            OnKeyPressCommand = new RelayCommand(OnKeyPressedAction);
            ((RelayCommand)OnKeyPressCommand).IsEnabled = true;
        }

        private void OnKeyPressedAction(object o)
        {
            var i = o as string;
            if (i != null && i.Length != 1) return;
            if(KeyPressed!=null)
                KeyPressed.Invoke(i[0]);
        }

        public ICommand OnKeyPressCommand { get; set; }

    }
}
