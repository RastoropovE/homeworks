﻿using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.List.DAL.Repository
{
    public class LogRepository : IRepository<LogDataModel>
    {
        private IList<LogDataModel> _logs = new List<LogDataModel>();

        public LogDataModel GetSingle(int id)
        {
            throw new NotImplementedException();
        }

        public IList<LogDataModel> GetAll()
        {
            return GetByQuery(p => true);
        }

        public int AddOrUpdate(LogDataModel model)
        {
            var log = _logs.Where(p => p.LogId == model.LogId);
            if (log == null)
            {
                model.LogId = _logs.Any() ? _logs.Max(p => p.LogId) + 1 : 1;
            }
            _logs.Add(model);
            return 1;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IList<LogDataModel> GetByQuery(Func<LogDataModel, bool> predicate, bool details = true)
        {
            var rez = _logs.Where(predicate).Select(p => p);
            return rez.ToList();
        }



    }
}
