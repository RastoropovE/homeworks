﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using C2.WPF.ViewModels;

namespace C2.WPF.Views
{
    /// <summary>
    /// Interaction logic for KeyBoard.xaml
    /// </summary>
    public partial class KeyBoard : UserControl
    {
        private static string nul = null;
        private string[][] _buttons = new[]
        {
            new[] {nul, nul, nul,"√","c"},
            new[] {"1", "2", "3","(","+"},
            new[] {"4", "5", "6",")","-"},
            new[] {"7", "8", "9",nul,"/"},
            new[] {"0", ",", "=",nul,"*"},
        };

        public KeyBoard()
        {
            InitializeComponent();
            DataContextChanged+=OnDataContextChanged;
        }

        private void OnDataContextChanged(object sender, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            Field.Children.Clear();
            for (var i = 0; i < _buttons[0].Length ; i++)
                Field.ColumnDefinitions.Add(new ColumnDefinition());
            for (var i = 0; i < _buttons.Length ; i++)
                Field.RowDefinitions.Add(new RowDefinition());

            for (var i = 0; i < _buttons.Length; i++)
                for (var j = 0; j < _buttons[0].Length; j++)
                {
                    if(_buttons[i][j]==null)continue;
                   
                    var b =new Button()
                    {
                        Command = ((BaseKeyBoard) Field.DataContext).OnKeyPressCommand,
                        Content = _buttons[i][j],
                        CommandParameter = (object)_buttons[i][j],
                        Margin = new Thickness(1),
                        FontSize = 36,
                    };
                    b.SetValue(Grid.RowProperty,i);
                    b.SetValue(Grid.ColumnProperty, j);
                    Field.Children.Add(b);

                }
        }

       
    }
}
