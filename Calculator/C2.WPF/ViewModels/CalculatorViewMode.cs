﻿using System.Collections.ObjectModel;
using Calc2.BL;
using Calc2.BL.DataModels;
using ScrumTaskBoard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace C2.WPF.ViewModels
{
    class CalculatorViewMode:BaseViewModel
    {
        public BaseKeyBoard KeyBoard { get; set; }


        private string _currentValue = string.Empty;

        public string CurrentValue
        {
            get { return _currentValue; }
            set
            {
                _currentValue = value;
                OnPropertyChanged("CurrentValue");
            }
        }


        public bool WriteLogs
        {
            get { return _calcModel.WriteLogs; }
            set
            {
                _calcModel.WriteLogs = value;
                OnPropertyChanged("WriteLogs");
            }
        }

        public ObservableCollection<LogDataModel> Logs
        {
            get { return _logs; }
            set
            {
                _logs = value; 
                OnPropertyChanged("Logs");
            }
        }

        private ResultDataModel _prevResult;
        public ResultDataModel PrevResult
        {
            get { return _prevResult; }
            set
            {
                _prevResult = value;
                OnPropertyChanged("PrevResult");
            }
        }


        private CalculatorService _calcModel;
        private ObservableCollection<LogDataModel> _logs;

        public CalculatorViewMode()
        {
            _calcModel =new  CalculatorService();
            KeyBoard = new BaseKeyBoard();
            KeyBoard.KeyPressed += OnKeyPressed;
            Logs = new ObservableCollection<LogDataModel>();
        }

        private void OnKeyPressed(string key)
        {
            if (key == "=")
            {
                Solve();
            }
            else if (key == "c")
            {
                CurrentValue = CurrentValue.Remove(CurrentValue.Length-1);
            }
            else
            {
                CurrentValue += key;
            }
        }

        private void Solve()
        {
            PrevResult = _calcModel.GetResult(CurrentValue);
            CurrentValue = string.Empty;
            Logs.Clear();
            foreach (var logDataModel in _calcModel.GetLogs().OrderByDescending(model => model.DateTime))
            {
                Logs.Add(logDataModel);
            }
        }




    }
}
