﻿using System;
using System.Windows.Input;
using ScrumTaskBoard.ViewModels;

namespace C2.WPF.ViewModels
{
    internal class BaseKeyBoard:BaseViewModel
    {
        public  delegate void OnKeyPressedEvent(string key);
        public event OnKeyPressedEvent KeyPressed;

        public BaseKeyBoard()
        {
            OnKeyPressCommand = new RelayCommand(OnKeyPressedAction);
            ((RelayCommand)OnKeyPressCommand).IsEnabled = true;
        }

        private void OnKeyPressedAction(object o)
        {
            var i = o as string;
            if (i == null) return;
            if(KeyPressed!=null)
                KeyPressed.Invoke(i);

        }

        public ICommand OnKeyPressCommand { get; set; }

        
    }



}
