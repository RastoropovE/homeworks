﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Calculator.WPF.ViewModels
{
    public class KeyBoardViewModel:BaseViewModel
    {
        #region Properties
        public string[][] KeyBoardData { get; set; }

        public ICommand ClickCommand { get; set; }

        #endregion

        #region events 
        public delegate void OnKeyClickHandler(object sender, OnKeyClickEventArgs args);
        public event OnKeyClickHandler KeyClicked;
        
        private void KeyClickedRaiseEvent(string key){
            var handler = KeyClicked;
            if (handler != null)
                handler.Invoke(this, new OnKeyClickEventArgs(key));
        }

        #endregion

        #region constructors

        public KeyBoardViewModel(string[][] keySet)
        {
            KeyBoardData = keySet;
            ClickCommand = new RelayCommand(ClickAction);
            ((RelayCommand) ClickCommand).IsEnabled = true;

        }

        #endregion

        public void ClickAction(object o)
        {
            var str = o as string;
            if (str != null) 
            KeyClickedRaiseEvent(str);
        }

    }

    public class OnKeyClickEventArgs : EventArgs
    {
        public string Key { get; private set; }

        public OnKeyClickEventArgs(string key)
        {
            Key = key;
        }
    }

}
