﻿using System.ComponentModel;
using Calculator.CommonBL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Calculator.WPF.ViewModels
{
    class AuthentificationViewModel:BaseViewModel,IDataErrorInfo
    {
        private readonly UserService _userservice;
        private bool _isWrongPAssword;

        public delegate void UserEventHandler();

        public event UserEventHandler CancelClicked;


        public event UserEventHandler OnUserEnter;


        #region properties
        public string Login { get; set; }

        public ICommand SubmitCommand { get; set; }

        public ICommand CancelCommand { get; set; }

        public bool IsWrongPassword
        {
            get { return _isWrongPAssword; }
            set
            {
                _isWrongPAssword = value;
                OnPropertyChanged("IsWrongPassword");
            }
        }

        #endregion

        public AuthentificationViewModel(UserService userservice)
        {
            _userservice = userservice;
            SubmitCommand = new RelayCommand(SumbitAction);
            ((RelayCommand)SubmitCommand).IsEnabled = true;
            CancelCommand = new RelayCommand(CancelAction);
            ((RelayCommand) CancelCommand).IsEnabled = true;
            IsWrongPassword = false;
        }

        

        protected virtual void SumbitAction(object obj)
        {
            var pb = obj as PasswordBox;
            if (pb == null) return;

            var user = _userservice.GetSingle(Login);
            if (user == null) return;
            IsWrongPassword = pb.Password != user.Password;

            if (IsWrongPassword) return;
            if (_userservice.SetCurrentUser(user.UserId))
            {

                var handler = OnUserEnter;
                if (handler != null)
                    OnUserEnter.Invoke();
            }
            else throw  new Exception("Some errors with authorisation");
        }

        protected virtual void CancelAction(object obj)
        {
            var handler = CancelClicked;
            if (handler != null) handler();
        }

        #region IDataErrorInfo implements

        public string Error
        {
            get { return null; }
        }

        public string this[string columnName]
        {
            get
            {
                string errorStr = null;
                switch (columnName)
                {
                    case "Login":
                        if (Login.Length < 3)
                        {
                            errorStr = "Login: min length is 3";
                            break;
                        }
                        if (_userservice.ChechLoginUniqness(Login))
                            errorStr = "Login: does not exsist";
                        break;

                }
                return errorStr;
            }
        }
        #endregion


    }
}
