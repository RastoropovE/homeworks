﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Services;

namespace Calculator.WPF.ViewModels
{
    class MainUserViewModel:BaseViewModel
    {

        private UserService _service;
        private UserDataModel _currentUser;
        private int _tabIndex;

        public delegate void CloseViewHandler();

        public event CloseViewHandler OnClose;



        public AuthentificationViewModel AuthentificationVm { get; set; }


        public RegistrationViewModel RegistrationVm { get; set; }


        public UserDataModel CurrentUser
        {
            get { return _currentUser; }
            set
            {
                _currentUser = value;
                OnPropertyChanged("CurrentUser"); 
            }
        }

        public int TabIndex
        {
            get { return _tabIndex; }
            set
            {
                _tabIndex = value;
                OnPropertyChanged("TabIndex");
            }
        }

       public  ICommand LogOutCommand { get; set; }


        public MainUserViewModel(UserService service)
        {
            RegistrationVm = new RegistrationViewModel(service);
            AuthentificationVm = new AuthentificationViewModel(service);

            _service = service;

            LogOutCommand = new RelayCommand(LogOutAction);
            ((RelayCommand) LogOutCommand).IsEnabled = true;

            RegistrationVm.OnUserEnter += AuthentificationVmOnOnUserEnter;
            AuthentificationVm.OnUserEnter+= AuthentificationVmOnOnUserEnter;

            RegistrationVm.CancelClicked += CloseAction;
            AuthentificationVm.CancelClicked += CloseAction;

            TabIndex = 1;
        }

        private void AuthentificationVmOnOnUserEnter()
        {
            CurrentUser = _service.CurrentUser;
            TabIndex = 0;
        }

        private void CloseAction()
        {
            var handler = OnClose;
            if (handler != null) handler();
        }

        private void LogOutAction(object obj)
        {
            _service.LogOut(CurrentUser.UserId);
            CurrentUser = null;
            TabIndex = 1;

        }
    }
}
