﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Services;
using System.Windows.Input;

namespace Calculator.WPF.ViewModels
{
    class RegistrationViewModel:AuthentificationViewModel,IDataErrorInfo
    {

        private UserService _userService;

        #region properties
        private string _firstName;
        private string _lastName;

        public string FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged("FirstName");
            }
        }
        public string LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged("LastName");
            }
        }
        #endregion properties


        public RegistrationViewModel(UserService userService):base(userService)
        {
            _userService = userService;
        }


        protected override void SumbitAction(object obj)
        {

            var pb = obj as PasswordBox;
            if (pb == null) return;

            if (this["Login"] != null) return;
            if (this["FirstName"] != null) return;
            if (this["LastName"] != null) return;
            
            var user = new UserDataModel()
            {
                Login = Login,
                Password = pb.Password,
                FirstName = FirstName,
                LastName = LastName,
            };

            if (!_userService.RegisterNewUser(user))
            {
                throw  new Exception("unexpected error with registration");
            }

            base.SumbitAction(obj);
        }

        #region IDataErrorInfo implements

        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public  string this[string columnName]
        {
            get
            {
                string errorStr = null;
                switch (columnName)
                {
                    case "FirstName":
                        if (FirstName.Length < 3)
                            errorStr = "First name: min length is 3";
                        break;
                    case "LastName":
                        if (LastName.Length < 3)
                            errorStr = "Last name: min length is 3";
                        break;
                    case "Login":
                        if (Login.Length < 3)
                            errorStr = "Login: min length is 3";
                        if (!_userService.ChechLoginUniqness(Login))
                            errorStr = "Login: is not unique";
                        break;

                }
                return errorStr;
            }
        }
        #endregion
    }
}
