﻿using Calculator.CommonBL.Core;
using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Services;
using Calculator.CommonBL.Services.Calculator;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.WPF.ViewModels
{
    internal class CalculatorViewModel : BaseViewModel
    {

        private string[][] keyBoard = new[]
        {
            new[] {null, null, "^", "(", ")"},
            new[] {null, null, null, "√", "c"},
            new[] {"1", "2", "3", "sin", "+"},
            new[] {"4", "5", "6", "cos", "-"},
            new[] {"7", "8", "9", "tan", "/"},
            new[] {"0", ",", "=", "ctg", "*"},
        };
        
        #region properties
        
        private KeyBoardViewModel _calculatorKeyBoard;

        private ObservableCollection<string> _mainDisplay;
        
        public KeyBoardViewModel CalculatorKeyBoard
        {
            get { return _calculatorKeyBoard; }
            set
            {
                _calculatorKeyBoard = value;
                OnPropertyChanged("CalculatorKeyBoard");
            }
        }

        public ObservableCollection<string> ExpressionElements
        {
            get { return _mainDisplay; }
            set
            {
                _mainDisplay = value;
                OnPropertyChanged("MainDisplay");
            }
        }

        private string _currentExpression;
        public string CurrentExpression
        {
            get { return _currentExpression; }
            set
            {
                _currentExpression = value;
                OnPropertyChanged("CurrentExpression");
            }
        }

        public ResultDataModel CurrentResult { get; set; }

        #endregion

        protected CalcWithMemory _calculator;

        public CalculatorViewModel()
        {
            CalculatorKeyBoard = new KeyBoardViewModel(keyBoard);
            ExpressionElements = new ObservableCollection<string>();

            CalculatorKeyBoard.KeyClicked += KeyClicked;
            _calculator = new CalcWithMemory(new StandartElementsFactory());
        }

        private void KeyClicked(object sender, OnKeyClickEventArgs args)
        {
            var key = args.Key;
            if (key.Equals("="))
            {
                SolveExpression();
            }
            else if (key.ToUpper().Equals("C"))
            {
                if (ExpressionElements.Count > 0)
                    ExpressionElements.RemoveAt(ExpressionElements.Count - 1);
                if(_currentExpression.Length >=1)
                CurrentExpression = CurrentExpression.Substring(0, _currentExpression.Length - 1);
            }
            else
                CurrentExpression += args.Key;
           // ExpressionElements.Add(args.Key);
        }

        private void SolveExpression()
        {
            //var rezult = _calculator.GetResult(CurrentExpression);
            CurrentExpression = _calculator.GetResult(CurrentExpression).ToString();
        }


    }
}
