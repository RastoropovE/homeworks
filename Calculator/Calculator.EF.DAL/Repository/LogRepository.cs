﻿using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Repository;
using Calculator.EF.DAL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.EF.DAL.Repository
{
    public class LogRepository : BaseRepository, IRepository<LogDataModel>
    {

        public LogDataModel GetSingle(int id)
        {
            var user = GetByQuery(p => p.LogId == id);
            return user==null ?null: user.FirstOrDefault();
        }

        public IList<LogDataModel> GetAll()
        {
            return GetByQuery(p => true);
        }

        public int AddOrUpdate(LogDataModel model)
        {
            using (var context = GetContext())
            {
                var log = context.Logs.FirstOrDefault(p => p.LogId == model.LogId);
                if (log == null)
                {
                    log = new Log();
                    model.LogId = NewKey(context);
                    context.Logs.Add(log);
                }
                log.CopyFromModel(model);

                return context.SaveChanges();
            }
        }
        
        public bool Delete(int id)
        {
            using (var context = GetContext())
            {
                var log = context.Logs.FirstOrDefault(p => p.LogId == id);
                if (log == null) return false;
                context.Logs.Remove(log);
                return context.SaveChanges() > 0;
            }
        }
        
        public IList<LogDataModel> GetByQuery(Func<LogDataModel, bool> predicate, bool details = true)
        {
            using (var context = GetContext())
            {
                var rez = context.Logs.Select(Log.ToModelFunc).Where(predicate).ToArray();
                foreach (var log in rez)
                {
                    log.User = context.Users.Where(p => p.UserId == log.UserId).Select(User.ToModelFunc).SingleOrDefault();
                }
                return rez.ToArray();
            }
        }


        private int NewKey(MultiuserCalcEntities context)
        {
            return context.Logs.Any() ? context.Logs.Max(p => p.LogId) + 1 : 1;
        }
    }
}
