﻿using Calculator.CommonBL.DataModels;
using Calculator.CommonBL.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.EF.DAL.Repository
{
    public class UsersRepository:BaseRepository, IRepository<UserDataModel>
    {

       public UserDataModel GetSingle(int id)
        {
            var us = GetByQuery(p => p.UserId == id);
            return us != null ? us.FirstOrDefault() : null;
        }

        public IList<UserDataModel> GetAll()
        {
            return GetByQuery(p => true);
        }

        public int AddOrUpdate(UserDataModel model)
        {
            using (var context = GetContext())
            {
                var usr = context.Users.FirstOrDefault(p => p.UserId == model.UserId);
                if (usr == null)
                {
                    usr = new User();
                    model.UserId = NewKey(context);
                    context.Users.Add(usr);
                }
                usr.CopyFromModel(model);

                return context.SaveChanges();
            }
        }

        private int NewKey(MultiuserCalcEntities context)
        {
            return context.Users.Any() ? context.Users.Max(p => p.UserId) + 1 : 1;
        }

        public bool Delete(int id)
        {
            using (var context = GetContext())
            {
                var project = context.Users.FirstOrDefault(p => p.UserId == id);
                if (project == null) return false;
                context.Users.Remove(project);
                return context.SaveChanges()>0;
            }
        }

        public IList<UserDataModel> GetByQuery(Func<UserDataModel, bool> predicate, bool details = true)
        {
           using(var context =  GetContext() )
           {
               var rez = context.Users.Select(User.ToModelFunc).Where(predicate).ToArray();
               foreach (var user in rez)
               {
                   user.Logs = context.Logs.Where(p => p.UserID == user.UserId).Select(Log.ToModelFunc).ToArray();
               }
               return rez;

           }
        }


    }
}
