﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.EF.DAL.Repository
{
    public abstract class BaseRepository
    {
        protected MultiuserCalcEntities GetContext()
        {
            return new MultiuserCalcEntities();
        }

    }
}
