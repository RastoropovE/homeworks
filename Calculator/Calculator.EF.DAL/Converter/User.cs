﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Calculator.CommonBL.DataModels;

namespace Calculator.EF.DAL
{
    public partial class User
    {
        public static Expression<Func<User, UserDataModel>> ToModelFunc = (e) => new UserDataModel()
        {
            FirstName = e.FirstName,
            LastName = e.LastName,
            Login = e.Login,
            Password = e.Password,
            UserId = e.UserId,
        };

     

        public void CopyFromModel(UserDataModel model)
        {
            FirstName = model.FirstName;
            LastName = model.LastName;
            Login = model.Login;
            Password = model.Password;
            UserId = model.UserId;
        }



    }
}
