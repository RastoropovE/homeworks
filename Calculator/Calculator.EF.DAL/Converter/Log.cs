﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Calculator.CommonBL.DataModels;

namespace Calculator.EF.DAL
{
    public partial class Log
    {
        public static Expression<Func<Log, LogDataModel>> ToModelFunc = (e) => new LogDataModel()
        {
            ErrorDetails = e.ErrorDetails,
            IsError = e.IsError ?? false,
            LogId = e.LogId,
            Message = e.Message,
            Rezult = e.Result ?? 0,
            UserId = e.UserID,
        };


        public void CopyFromModel(LogDataModel model)
        {
            ErrorDetails = model.ErrorDetails??"";
            IsError = model.IsError;
            LogId = model.LogId;
            Message = model.Message;
            Result = model.Rezult;
            DateTime = model.DateTime;
        }


    }
}
