﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calc1.BL.Operations;
using Calc1.BL.Operations.Common;

namespace Calc1.BL
{
   public  class CalculatorService
    {
        public  bool  EnableLog { get; set; }
        

        public CalculatorService(bool writeLogs)
        {
            EnableLog = writeLogs;
            _operations = new OperationFactory();
            Calculator = new Calculator();
        }

        #region  Calculatations;

        public Calculator Calculator { get; set; }
        private OperationFactory _operations;
        public string Action(double number)
        {
            return Calculator.SetValue(number).ToString();
        }

        public string Action(Func<OperationFactory, Operation> opFunc)
        {
            var op = opFunc(_operations);
            return Calculator.SetValue(op).ToString();
        }

        #endregion

    }
}
