﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Calc1.BL.Operations;
using Calc1.BL.Operations.Common;
using Calc1.BL.Operations.Standart;

namespace Calc1.BL
{
    public class Calculator
    {
        public double? PreviousResult { get; set; }
        public Operation CurrentOperation { get; private set; }
        private double? CurrentValue { get; set; }


        public double SetValue(double number)
        {
            if (CurrentValue.HasValue)
            {
                PreviousResult = CurrentOperation.Solve(CurrentValue.Value, number);
                CurrentValue = null;
                return PreviousResult.Value;
            }
            CurrentValue = number;
            return 0;
        }

        public double SetValue(Operation operation)
        {
            if (!operation.IsBinary && CurrentValue.HasValue)
            {
                PreviousResult =  operation.Solve(CurrentValue.Value);
                CurrentValue = null;
                return PreviousResult.Value;
            }
            CurrentOperation = operation;
            return 0;
        }

        public double GetResult(double arg1)
        {
            return CurrentOperation.Solve(arg1);
        }

        public double GetResult(double arg1, double arg2)
        {
            return CurrentOperation.Solve(arg1, arg2);
        }

        public void SetAction(Func<OperationFactory, Operation> opFunc)
        {
            CurrentOperation = opFunc(OperationFactory.Instance);
        }


    }
}
