﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calc1.BL.Operations.Common;
using Calc1.BL.Operations.Standart;

namespace Calc1.BL.Operations
{
    public enum SimpleOperation
    {
        Add,Sub,Div,Mul
    }
    public class OperationFactory
    {
        private static OperationFactory _instance;


        public Operation GetSimple( SimpleOperation opType)
        {
            Operation op = null;
            switch (opType)
            {
                case SimpleOperation.Add: 
                    op = new AddOperation();
                    break;
                case SimpleOperation.Sub:
                    op = new SubOperation();
                    break;
                case SimpleOperation.Div:
                    op = new DivOperation();
                    break;
                case SimpleOperation.Mul:
                    op = new MulOperation();
                    break;
            }
            return op;
        }


        public static OperationFactory Instance
        {
            get
            {
                _instance = _instance ?? new OperationFactory();
                return _instance;
            }
        }
    }
}
