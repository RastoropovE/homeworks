﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calc1.BL.Operations.Common;

namespace Calc1.BL.Operations.Standart
{
   public  class MulOperation:Operation
    {
        public override double Solve(params double[] args)
        {
            return args.Count() == 0 ? 0 : args.Sum();
        }

       public MulOperation()
       {
           IsBinary = true;
       }

    }
}
