﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calc1.BL.Operations.Common;

namespace Calc1.BL.Operations.Standart
{
   public class DivOperation:Operation
    {
        public override double Solve(params double[] args)
        {
            return args.Count() != 2 ? 0 : args[0]/args[1];
        }

       public DivOperation()
       {
           IsBinary = true;
       }

    }
}
