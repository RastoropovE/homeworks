﻿namespace Calc1.BL.Operations.Common
{
    public abstract class Operation
    {
        public bool IsBinary { get; protected set; }

        public abstract double Solve(params double[] args);



    }
}
