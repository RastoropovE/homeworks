﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Calculator.MVC.ASP2.Startup))]
namespace Calculator.MVC.ASP2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
