﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calc2.BL.DataModels;
using Calc2.BL.Log;

namespace Calc2.BL
{
    public class CalculatorService
    {
        #region Logs
        IRepository<LogDataModel> LogRepository { get; set; }
        public bool WriteLogs { get; set; }

        #endregion

        private readonly Calculator _calculator;

        public CalculatorService()
        {
            WriteLogs = true;
            LogRepository = new LogRepository();
            _calculator = new Calculator();
        }

        public IEnumerable<LogDataModel> GetLogs()
        {
            return LogRepository.GetQuery(p => true);
        } 


        public ResultDataModel GetResult(string expression)
        {
            var rez = _calculator.GetResult(expression);
            if (WriteLogs) LogRepository.AddOrUpdate(new LogDataModel(rez));
            return rez;
        }

        private static CalculatorService _instance;
        public static CalculatorService GetInstance
        {
            get
            {
                _instance = _instance ?? new CalculatorService();
                return _instance;
            }
        }


    }
}
