﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Calc2.BL.DataModels;
using Calc2.BL.PNE;

namespace Calc2.BL
{
    class Calculator
    {

        private ExpressionConverter _calcCore;

        public Calculator()
        {
            _calcCore = new ExpressionConverter() ;
        }

        public ResultDataModel GetResult(string line)
        {
            var rez = new ResultDataModel();
            rez.Message = line;
            try
            {
              rez.Rezult=  _calcCore.Result(line);
            }
            catch (Exception e)
            {
                rez.IsError = true;
            }
            return rez;

        }


    }
}
