﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc2.BL.Log
{
    interface IRepository<TDataModel>
    {

        IList<TDataModel> GetAll();

        IList<TDataModel> GetQuery(Func<TDataModel,bool> queryFunc );

        int Delete(TDataModel obj);
        int AddOrUpdate(TDataModel obj);




    }
}
