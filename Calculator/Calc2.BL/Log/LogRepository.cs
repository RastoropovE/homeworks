﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Calc2.BL.DataModels;

namespace Calc2.BL.Log
{
    class LogRepository:IRepository<LogDataModel>
    {
        private IList<LogDataModel> _logs;


        #region
        public IList<LogDataModel> GetAll()
        {
            return GetQuery(p => true);
        }

        public IList<LogDataModel> GetQuery(Func<LogDataModel, bool> queryFunc)
        {
            _logs = DeserializeLogs();
            return _logs.Where(queryFunc).Select(p => p).ToList();
        }

        public int Delete(LogDataModel obj)
        {
            _logs = DeserializeLogs();
            _logs.Remove(obj);
            SerializeLogs(_logs);
            return -1;
        }

        public int AddOrUpdate(LogDataModel obj)
        {
            _logs = DeserializeLogs();
           var l =  _logs.FirstOrDefault(p => p.Id == obj.Id);
            if (l == null)
            {
                obj.Id = _logs.Any() ? _logs.Max(p => p.Id) + 1 : 1;
                _logs.Add(obj);
            }
            else
            {
                _logs.Remove(l);
                _logs.Add(obj);
            }

            SerializeLogs(_logs);
            return -1;
        }


        #endregion

        private string _path = @"../../l.dat";
        private IList<LogDataModel> DeserializeLogs()
        {
            using (var fs = new FileStream(_path,FileMode.OpenOrCreate))
            {
                if (fs.Length == 0) return new List<LogDataModel>();
                var bs = new BinaryFormatter();
                return (List<LogDataModel>)bs.Deserialize(fs);
            }
        }

        private void SerializeLogs(IList<LogDataModel> list)
        {
            using (var fs = new FileStream(_path, FileMode.OpenOrCreate))
            {
                var bs = new BinaryFormatter();
                bs.Serialize(fs,list);
            }
        }
    }
}
