﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc2.BL.PNE
{
    class PostfixElementsFactory
    {
        //          priority                 left - min                             right - max
        private readonly String[] _operators = new[] { "(", ")", "+", "-", "*", "/", "^", "√" };
        public string[] Operators
        {
            get { return _operators; }
        }


        Dictionary<string,PostfixElement>  elements = new Dictionary<string, PostfixElement>()
        {
             {"(", new PostfixElement(true,0) },
             {")", new PostfixElement(false,0) },

             {"+", new PostfixElement(2, 1){ Operation = d => d[0] + d[1]  } },
             {"-", new PostfixElement(2, 1){ Operation = d => d[1] - d[0]  } },
             {"/", new PostfixElement(2, 2){ Operation = d => d[1] / d[0]  } },
             {"*", new PostfixElement(2, 2){ Operation = d => d[0] * d[1]  } },

             {"^", new PostfixElement( 2, 3){Operation = d => Math.Pow(d[0], d[1])} },

             // unary
             {"√", new PostfixElement( 1, 4){Operation = d => Math.Sqrt(d[0])} }, 

        };

        public PostfixElementsFactory()
        {
            
        }

        public  PostfixElement GetElement(string p)
        {
            if (_operators.Contains(p)) //operator
            {
                var postfixElement = GetOperator(p);
                if (postfixElement != null) return postfixElement;
            }
            else
            {
                var val = double.Parse(p);
                return new PostfixElement(val);
            }
            return null;
        }

        private PostfixElement GetOperator(string p)
        {
            var rex = elements.Where(pair => pair.Key == p).Select(pair => pair.Value);
            return rex.SingleOrDefault();
        }
    }
}
