﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc2.BL.PNE
{
   public  class ExpressionConverter
   {
       private PostfixElementsFactory _pr = new PostfixElementsFactory();


        private IEnumerable<string> Separate(string input)
        {
            var pos = 0;
            while (pos < input.Length)
            {
                var s = string.Empty + input[pos];
                if (!_pr.Operators.Contains(input[pos].ToString()))
                {
                    if (Char.IsDigit(input[pos]))
                        for (int i = pos + 1; i < input.Length &&
                            (Char.IsDigit(input[i]) || input[i] == ',' || input[i] == '.'); i++)
                            s += input[i];
                    else if (Char.IsLetter(input[pos]))
                        for (var i = pos + 1; i < input.Length &&
                            (Char.IsLetter(input[i]) || Char.IsDigit(input[i])); i++)
                            s += input[i];
                }
                yield return s;
                pos += s.Length;
            }
        }

        public PostfixElement[] ConvertToPostfixNotation(string input)
        {
            var outputSeparated = new List<PostfixElement>();
            var stack = new Stack<PostfixElement>();
            foreach (var c in Separate(input).Select(p=>_pr.GetElement(p)))
            {
                if (c.ElementType==ElType.Operator)
                {
                    if (stack.Count > 0 && c.ElementType != ElType.LeftB)
                    {
                        if (c.ElementType == ElType.RightB)
                        {
                            var s = stack.Pop();
                            while (s.ElementType!=ElType.LeftB)
                            {
                                outputSeparated.Add(s);
                                s = stack.Pop();
                            }
                        }
                        else if (c.Priority > stack.Peek().Priority)
                            stack.Push(c);
                        else
                        {
                            while (stack.Count > 0 && c.Priority <= stack.Peek().Priority)
                                outputSeparated.Add(stack.Pop());
                            stack.Push(c);
                        }
                    }
                    else
                        stack.Push(c);
                }
                else
                    outputSeparated.Add(c);
            }
            if (stack.Count > 0)
                outputSeparated.AddRange(stack);

            return outputSeparated.Where(p => p.ElementType != ElType.LeftB).Where(p => p.ElementType != ElType.RightB).Select(p => p).ToArray();
        }


        public double Result(string input)
        {
            var stack = new Stack<PostfixElement>();
            var queue = new Queue<PostfixElement>(ConvertToPostfixNotation(input));
            var str = queue.Dequeue();
            while (queue.Count >= 0)
            {
                if (str.ElementType!=ElType.Operator)
                {
                    stack.Push(str);
                    str = queue.Dequeue();
                }
                else
                {
                    double summ = 0;
                    try
                    {
                        switch (str.Operands)
                        {
                            case 1:
                                {
                                    var a =stack.Pop().Value;
                                    summ = str.Operation(new []{a});
                                    break;
                                }
                            case 2:
                                {
                                    var a = stack.Pop().Value;
                                    var b = stack.Pop().Value;
                                    summ = str.Operation(new [] { a,b });
                                    break;
                                }    
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("fu");
                    }
                    stack.Push(new PostfixElement(summ));
                    if (queue.Count > 0)
                        str = queue.Dequeue();
                    else
                        break;
                }
            }
            return stack.Pop().Value;
        }

    }
}
