﻿using System;

namespace Calc2.BL.PNE
{
    public enum ElType {Operator,Operand,LeftB,RightB }
    public class  PostfixElement
    {

        public ElType ElementType { get; set; }
        private readonly int _operands;
        
        public int Operands
        {
            get { return _operands; }
        }

        private readonly int _priority;
        public int Priority
        {
            get { return _priority; }
        }

        public double Value { get; set; }
        public Func<double[], double> Operation { get; internal set; }


        #region Constructors

        /// <summary>
        /// Use it for operators
        /// You have to set operation
        /// </summary>
        /// <param name="operands"></param>
        /// <param name="priority"></param>
        public PostfixElement(int operands, int priority)
        {
            ElementType  = ElType.Operator;
            _operands = operands;
            _priority = priority;

        }
        /// <summary>
        /// Use it for operands
        /// </summary>
        /// <param name="value"></param>
        public PostfixElement(double value)
        {
            ElementType = ElType.Operand;
            Value = value;
        }
        /// <summary>
        /// Use it for brakets
        /// </summary>
        public PostfixElement(bool isLeft, int priority)
        {
            ElementType = isLeft ? ElType.LeftB : ElType.RightB;
        }

        #endregion

    }
}