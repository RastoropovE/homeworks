﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc2.BL.DataModels
{
    [Serializable]
    public class LogDataModel
    {
        public int Id { get; set; }

        private readonly ResultDataModel _result;

        public bool IsError
        {
            get { return _result.IsError; }
        }

        public string Message
        {
            get { return _result.Message; }
        }

        public double Rezult
        {
            get { return _result.Rezult; }
        }
        public DateTime DateTime { get; private set; }
        public LogDataModel(ResultDataModel result)
        {
            if(result==null) throw  new ArgumentNullException("result");
            _result = result;
            DateTime = DateTime.Now;
        }

        public override string ToString()
        {
            return string.Format("{0}({1}) {2} = {3}", Id, this.DateTime, Message,IsError?"Error": Rezult.ToString());
        }
    }

}
