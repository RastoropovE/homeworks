﻿var app = angular.module('CalculatorApp', []);

app.controller('Main', function ($scope) {


    $scope.MemoryValue = 0;
    $scope.BufDisplay = "0";
    $scope.CurDisplay = "0";

    var previousResults = [];

    /*    Commans   */
    //binary
    var add = function (val1, val2) { return parseFloat(val1) + parseFloat(val2); }
    var sub = function (val1, val2) { return parseFloat(val1) - parseFloat(val2); }
    var mul = function (val1, val2) { return parseFloat(val1) * parseFloat(val2); }
    var div = function (val1, val2) { return parseFloat(val1) / parseFloat(val2); }
    //unary
    var sqr = function () { $scope.CurDisplay = Math.sqrt(parseFloat($scope.CurDisplay)).toString(); }
    var prs = function () { $scope.CurDisplay= parseFloat($scope.CurDisplay) / 100; }
    var onedivx = function () { $scope.CurDisplay = 1 / parseFloat($scope.CurDisplay); }
    var invert = function () { $scope.CurDisplay = -parseFloat($scope.CurDisplay);  }
    //special
    var equ = function () {
        if (isResult) return;
        $scope.CurDisplay = curCommand($scope.BufDisplay, $scope.CurDisplay);
        $scope.BufDisplay = "0";
        curCommand = empty;
        previousResults.push(parseFloat($scope.CurDisplay));
        isResult = true;
    }

    var memplus = function () { $scope.MemoryValue += parseFloat($scope.CurDisplay); }
    var memminus = function () { $scope.MemoryValue -= parseFloat($scope.CurDisplay); }
    var memclear = function () { $scope.MemoryValue = 0; }
    var memget = function () { $scope.CurDisplay = $scope.MemoryValue; }

    var empty = function (val1, val2) { return val2; };
    var grandtotal = function() {
        var c = 0;
        for (var i = 0; i < previousResults.length; i++) c += previousResults[i];
        $scope.CurDisplay = c;
    };
    var bcsp = function () {
        $scope.CurDisplay = $scope.CurDisplay.toString();
        $scope.CurDisplay = $scope.CurDisplay.substr(0, $scope.CurDisplay.length - 1);
        if ($scope.CurDisplay.length == 0) $scope.CurDisplay = "0";
    };
    /* -end commands*/

    var isResult = false;
    var curCommand = empty;

    $scope.KeyBoard = [
        ['%', '1/x', '+/-', 'CE','GT', '='],
        ['M+', 'M-', 'MR', 'MC','C','←'],
        ['1', '2', '3', '+','√'],
        ['4', '5', '6', '-','*'],
        ['7', '8', '9', '/'],
        ['0', '.']

        //   CE(cancel last action)  GT
    ];
    
    $scope.AddNumber = function(ch) {
        if (isDigid(ch)) {
            if ($scope.CurDisplay == "0" || isResult) {
                $scope.CurDisplay = "";
                isResult = false;
            }
            $scope.CurDisplay += ch;
        } else {
            switch (ch) {
                case "=": equ();  break;
                case "-": setNewCommand(sub); break;
                case "+": setNewCommand(add); break;
                case "/": setNewCommand(div); break;
                case "*": setNewCommand(mul); break;

                case "√": sqr(); break;
                case '←': bcsp(); break;
                case "1/x": onedivx(); break;
                case "+/-": invert(); break;
                case "%": prs(); break;

                case "M+": memplus(); break;
                case "M-":memminus();break;
                case "MR": memget(); break;
                case "MC": memclear(); break;

                case "GT":grandtotal(); break;

            }
        }
        console.log($scope.BufDisplay);
        console.log(ch);

    };

    

    var setNewCommand = function(cmd) {
        $scope.BufDisplay = curCommand($scope.BufDisplay, $scope.CurDisplay);
        $scope.CurDisplay = "0";
        curCommand = cmd;
    }



    var isDigid = function (c){return c.length==1 &&( c >= '0' && c <= '9' || c == '.');}

});
