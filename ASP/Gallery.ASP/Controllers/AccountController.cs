﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Gallery.ASP.Models;

namespace Gallery.ASP.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel user)
        {
            if (ModelState.IsValid)
            {
                Membership.ValidateUser(user.Email, user.Password);

               // HttpContext.
            }

            return View(user);
        }


        public ActionResult Registration()
        {
            return View();
        }


        public ActionResult Registration(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {


            }
            return View(model);
        }


	}
}