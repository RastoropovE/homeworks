﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Client.ASP.Startup))]
namespace Client.ASP
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
